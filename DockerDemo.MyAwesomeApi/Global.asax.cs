﻿using System.Web;
using System.Web.Http;

namespace DockerDemo.MyAwesomeApi
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure( x => x.MapHttpAttributeRoutes());
        }
    }
}