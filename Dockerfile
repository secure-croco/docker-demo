FROM microsoft/iis

SHELL ["powershell"]

RUN Install-WindowsFeature NET-Framework-45-ASPNET ; \
    Install-WindowsFeature Web-Asp-Net45

COPY PowerShell-Modules PowerShell-Modules
COPY PowerShell-Scripts Powershell-Scripts
COPY DockerDemo.MyAwesomeApi site

RUN ./PowerShell-Scripts/Install-Modules.ps1

RUN Remove-WebSite -Name 'Default Web Site'
RUN New-Website \
    -Name 'site' \
    -Port 80 \
    -PhysicalPath 'c:\site' \
    -ApplicationPool '.NET v4.5'
	
EXPOSE 80

ENTRYPOINT ["C:\\ServiceMonitor.exe", "w3svc"]
