## What is this?

This project is meant to demonstrate the basic uses of Docker for Windows.

- It builds a simple demo ASP.NET web API.
- It hosts the API on a Windows Server 2016 (Server Core) Docker container.

*Note: I would have prefered to use Nano Server, but so far, I had too many issues making that happen.*

## How to use?

1. Enable "Intel Virtualization Technology" in your BIOS.
2. Download and install Docker for Windows: https://download.docker.com/win/stable/InstallDocker.msi
3. Switch Docker to Windows mode by right-clicking on the Docker icon in your task tray (bottom right of the screen).
4. Download this repository as zip or clone it with Git.
5. Build `DockerDemo.sln` with Visual Studio 2017 (community edition can be used).
6. Open a *regular* PowerShell window. **IMPORTANT: PowerShell ISE can't be used with Docker!**
7. Navigate to the folder where `Docker.ps1` is, and run it.
8. The first-time execution is quite slow, as it needs to pull a Windows Server Core image **(~4GB download!)** from the Docker Store.
9. Once execution is done, copy the IP displayed in the PowerShell window.
10. In a web browser, go to `copied_ip/api/echo` to enjoy your resulting API running in Docker.

## Sources of help / inspiration

##### Official guides from Microsoft
- https://docs.microsoft.com/en-us/virtualization/windowscontainers/quick-start/quick-start-windows-10
- https://docs.microsoft.com/en-us/aspnet/mvc/overview/deployment/docker-aspnetmvc
- https://github.com/Microsoft/iis-docker

##### Official Docker images from Microsoft
- https://hub.docker.com/r/microsoft/iis/
- https://hub.docker.com/r/microsoft/aspnet/

##### Third-party tutorials and help
- http://blog.alexellis.io/run-iis-asp-net-on-windows-10-with-docker/
- https://github.com/alexellis/guidgenerator-aspnet
- https://blog.sixeyed.com/1-2-3-iis-running-in-nano-server-in-docker-on-windows-server-2016/
- https://medium.com/@felipedutratine/host-a-net-webapi-on-docker-on-a-nano-server-93a9dcec643e

##### Rooting out bad practices
- https://jpetazzo.github.io/2014/06/23/docker-ssh-considered-evil/

##### Help with Docker errors on Windows
- https://rominirani.com/docker-for-windows-startup-errors-fb5903431eda

##### Cleanup script from Microsoft
- https://github.com/MicrosoftDocs/Virtualization-Documentation/tree/master/windows-server-container-tools/CleanupContainerHostNetworking

## License

Please see the [LICENSE](LICENSE) file.
